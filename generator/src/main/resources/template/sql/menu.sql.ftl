<#assign dbTime = "now()">
<#if dbType=="SQLServer">
    <#assign dbTime = "getDate()">
</#if>

-- 菜单 SQL
INSERT INTO `sys_menu` (menu_name, parent_id, order_num, url, menu_type, visible, perms, component, redirect, icon, create_by, create_time, update_by, update_time, remark)
VALUES ('${tableComment}', 1 , 1, '/${moduleName}/${functionName}', 'C', '0', '${moduleName}:${functionName}:list', '${functionName?cap_first}', NULL, '', 'admin', sysdate(), '', NULL, '${tableComment}菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();


-- 按钮 SQL
INSERT INTO `sys_menu` (menu_name, parent_id, order_num, url, menu_type, visible, perms, component, redirect, icon, create_by, create_time, update_by, update_time, remark)
VALUES ('${tableComment}查询', @parentId, 1, '#', 'F', '', '${moduleName}:${functionName}:query', NULL, NULL, '', 'admin', sysdate(), '', NULL, '${tableComment}查询');

INSERT INTO `sys_menu` (menu_name, parent_id, order_num, url, menu_type, visible, perms, component, redirect, icon, create_by, create_time, update_by, update_time, remark)
VALUES ('${tableComment}新增', @parentId, 2, '#', 'F', '', '${moduleName}:${functionName}:add', NULL, NULL, '', 'admin', sysdate(), '', NULL, '${tableComment}新增');

INSERT INTO `sys_menu` (menu_name, parent_id, order_num, url, menu_type, visible, perms, component, redirect, icon, create_by, create_time, update_by, update_time, remark)
VALUES ('${tableComment}修改', @parentId, 3, '#', 'F', '', '${moduleName}:${functionName}:edit', NULL, NULL, '','admin', sysdate(), '', NULL, '${tableComment}修改');

INSERT INTO `sys_menu` (menu_name, parent_id, order_num, url, menu_type, visible, perms, component, redirect, icon, create_by, create_time, update_by, update_time, remark)
VALUES ('${tableComment}删除', @parentId, 4, '#', 'F', '', '${moduleName}:${functionName}:remove', NULL, NULL, '', 'admin', sysdate(), '', NULL, '${tableComment}删除');
