<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<mapper namespace="${package}.${moduleName}.mapper.${ClassName}Mapper">

    <resultMap type="${package}.${moduleName}.domain.${ClassName}" id="${className}Result">
        <#list fieldList as field>
            <#if field.primaryPk>
        <id property="${field.attrName}" column="${field.fieldName}"/>
            <#else>
        <result property="${field.attrName}" column="${field.fieldName}"/>
            </#if>
        </#list>
    </resultMap>

    <sql id="Base_Column_List">
        <#list fieldList as field>${field.fieldName}<#if field_has_next>,</#if></#list>
    </sql>

    <select id="select${ClassName}ById" parameterType="${primaryList[0].attrType}" resultMap="${className}Result">
        select
        <include refid="Base_Column_List"/>
        from ${tableName}
        where ${primaryList[0].fieldName} = <#noparse>#</#noparse>{${primaryList[0].attrName}}
    </select>

    <select id="select${ClassName}List" parameterType="${package}.${moduleName}.domain.${ClassName}" resultMap="${className}Result">
        select
        <include refid="Base_Column_List"/>
        from ${tableName}
        <where>
            <#list fieldList as field>
            <if test="${field.attrName} != null<#if field.attrType=='String'> and ${field.attrName}.trim() != ''</#if>">and ${field.fieldName} = <#noparse>#</#noparse>{${field.attrName}}</if>
            </#list>
        </where>
    </select>

    <insert id="insert${ClassName}" parameterType="${package}.${moduleName}.domain.${ClassName}" useGeneratedKeys="true" keyProperty="${primaryList[0].attrName}">
        insert into ${tableName}
        <trim prefix="(" suffix=")" suffixOverrides=",">
            <#list fieldList as field>
            <if test="${field.attrName} != null<#if field.attrType=='String'> and ${field.attrName}.trim() != ''</#if>">${field.fieldName},</if>
            </#list>
        </trim>
        <trim prefix="values (" suffix=")" suffixOverrides=",">
            <#list fieldList as field>
            <if test="${field.attrName} != null<#if field.attrType=='String'> and ${field.attrName}.trim() != ''</#if>"><#noparse>#</#noparse>{${field.attrName}},</if>
            </#list>
        </trim>
    </insert>

    <update id="update${ClassName}ById" parameterType="${package}.${moduleName}.domain.${ClassName}">
        update ${tableName}
        <trim prefix="SET" suffixOverrides=",">
            <#list fieldList as field>
            <if test="${field.attrName} != null<#if field.attrType=='String'> and ${field.attrName}.trim() != ''</#if>">${field.fieldName} = <#noparse>#</#noparse>{${field.attrName}},</if>
            </#list>
        </trim>
        where ${primaryList[0].fieldName} = <#noparse>#</#noparse>{${primaryList[0].attrName}}
    </update>

    <delete id="delete${ClassName}ById" parameterType="${primaryList[0].attrType}">
        delete from ${tableName} where ${primaryList[0].fieldName} = <#noparse>#</#noparse>{${primaryList[0].attrName}}
    </delete>

    <delete id="batchDelete${ClassName}ByIds" parameterType="${primaryList[0].attrType}">
        delete from ${tableName} where ${primaryList[0].fieldName} in
        <foreach item="${primaryList[0].attrName}" collection="array" open="(" separator="," close=")">
            <#noparse>#</#noparse>{${primaryList[0].attrName}}
        </foreach>
    </delete>
</mapper>