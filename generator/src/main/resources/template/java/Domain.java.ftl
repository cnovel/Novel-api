package ${package}.${moduleName}.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serial;
<#list importList as i>
import ${i!};
</#list>
<#if baseClass??>
import ${baseClass.packageName}.${baseClass.code};
<#else>
import java.io.Serializable;
</#if>

/**
 * ${tableComment}
 *
 * @author ${author}
 * @since ${datetime}
 */
<#if baseClass??>@EqualsAndHashCode(callSuper = false)</#if>
@Data
public class ${ClassName}<#if baseClass??> extends ${baseClass.code}<#else>implements Serializable</#if> {
	@Serial
	private static final long serialVersionUID = ${.now?long?replace(",","")}L;
<#list fieldList as field>
<#if !field.baseField>
	<#if field.fieldComment!?length gt 0>
	/**
	 * ${field.fieldComment}
	 */
	</#if>
	private ${field.attrType} ${field.attrName};
</#if>
</#list>
}