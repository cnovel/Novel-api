package ${package}.${moduleName}.service;

import ${package}.${moduleName}.domain.${ClassName};
import java.util.List;

/**
 * ${tableComment} Service接口
 *
 * @author ${author}
 * @since ${datetime}
 */
public interface I${ClassName}Service {
    /**
     * 查询${functionName}信息
     *
     * @param ${primaryList[0].fieldName} ${primaryList[0].fieldComment}
     * @return ${functionName}信息
     */
    ${ClassName} select${ClassName}ById(${primaryList[0].attrType} ${primaryList[0].fieldName});

    /**
     * 查询${functionName}列表
     *
     * @param ${className} ${functionName}信息
     * @return ${functionName}集合
     */
    List<${ClassName}> select${ClassName}List(${ClassName} ${className});

    /**
     * 新增${functionName}
     *
     * @param ${className} ${functionName}信息
     * @return 结果
     */
    boolean insert${ClassName}(${ClassName} ${className});

    /**
     * 修改${functionName}
     *
     * @param ${className} ${functionName}信息
     * @return 结果
     */
    boolean update${ClassName}ById(${ClassName} ${className});

    /**
     * 保存${functionName}
     *
     * @param ${className} ${functionName}信息
     * @return 结果
     */
    boolean save${ClassName}(${ClassName} ${className});

    /**
     * 删除${functionName}信息
     *
     * @param ${primaryList[0].fieldName} ${primaryList[0].fieldComment}
     * @return 结果
     */
    boolean delete${ClassName}ById(${primaryList[0].attrType} ${primaryList[0].fieldName});

    /**
     * 批量删除${functionName}信息
     *
     * @param ${primaryList[0].fieldName}s 需要删除的${primaryList[0].fieldComment}集合
     * @return 结果
     */
    boolean batchDelete${ClassName}(${primaryList[0].attrType}[] ${primaryList[0].fieldName}s);

}