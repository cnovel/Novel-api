import http from '@/utils/http';


/**
 * 获取 ${tableComment} 列表
 * @param params 参数
 * @returns {Promise<any>}
 */
export const get${ClassName}List = (params?: any): Promise<any> => {
	return http.get('${moduleName}/${functionName}/list', {params: params});
}

/**
 * 删除 ${tableComment}
 * @param params 参数
 * @returns {Promise<any>}
 */
export const remove${ClassName} = (params: any): Promise<any> => {
	return http.delete('${moduleName}/${functionName}/remove', {params: params});
}

/**
 * 修改 ${tableComment}
 * @param params 参数
 * @returns {Promise<any>}
 */
export const update${ClassName} = (params: any): Promise<any> => {
	return http.put('${moduleName}/${functionName}/edit', params);
}

/**
 * 新增 ${tableComment}
 * @param params 参数
 * @returns {Promise<any>}
 */
export const add${ClassName} = (params: any): Promise<any> => {
	return http.post("${moduleName}/${functionName}/add", params);
}