<template>
	<div class="tableRow">
		<el-row class="select-list" v-permission="'${moduleName}:${functionName}:query'">
			<el-form :inline="true" ref="selectForm" :model="selectFormModel" label-position="right"
					 label-width="80px" size="mini" @submit.native.prevent >
				<#list queryList as field>
				<el-form-item label="${field.fieldComment}" prop="${field.attrName}">
					<#if field.queryFormType == 'text' || field.queryFormType == 'textarea' || field.queryFormType == 'editor'>
					<el-input v-model="selectFormModel.${field.attrName}" placeholder="请输入${field.fieldComment}" style="width: 200px" clearable @keyup.enter.native="onSearch"/>
					<#elseif field.queryFormType == 'select'>
						<el-select v-model="selectFormModel.${field.attrName}" placeholder="请选择${field.fieldComment}" clearable size="small">
							<el-option
									v-for="option in optionData"
									:key="option.value"
									:label="option.label"
									:value="option.value"
							/>
						</el-select>
					<#elseif field.queryFormType == 'datetime'>
						<el-date-picker
							clearable size="small"
							v-model="selectFormModel.${field.attrName}"
							type="datetimerange"
							value-format="YYYY-MM-DD HH:mm:ss"
							placeholder="选择${field.fieldComment}">
						</el-date-picker>
					<#elseif field.queryFormType == 'date'>
						<el-date-picker
							clearable size="small"
							v-model="selectFormModel.${field.attrName}"
							type="daterange"
							value-format="YYYY-MM-DD"
							placeholder="选择${field.fieldComment}">
						</el-date-picker>
					</#if>
				</el-form-item>
				</#list>
				<el-form-item>
					<el-button type="primary" icon="el-icon-search" size="mini" plain round @click="onSearch">搜索</el-button>
					<el-button type="warning" icon="el-icon-refresh" size="mini" plain round @click="onRefreshSelectForm">重置</el-button>
				</el-form-item>
			</el-form>
		</el-row>
		<el-row class="toolbar">
			<el-button type="primary" icon="el-icon-plus" size="mini" plain @click="handleAdd" v-permission="'${moduleName}:${functionName}:add'">
				新增
			</el-button>
			<el-button type="info" icon="el-icon-edit" :disabled="dialog.isBatchEditDisabled" size="mini" plain @click="handleEdit" v-permission="'${moduleName}:${functionName}:edit'">
				编辑
			</el-button>
			<el-button type="danger" icon="el-icon-delete" :disabled="dialog.isBatchRemoveDisabled" size="mini" plain @click="handleBatchDelete" v-permission="'${moduleName}:${functionName}:remove'">
				删除
			</el-button>
		</el-row>
		<data-table ref="dataTable"
					:options="options"
					:operates="operates"
					:api="get${ClassName}List"
					:columns="columns"
					:query="selectFormModel"
					@handleSelectionChange="handleSelectionChange"
		/>

		<el-dialog :title="dialog.title"
				   :visible.sync="dialog.dialogFormVisible"
				   :modal-append-to-body="false"
				   @close="closeDialog"
				   :destroy-on-close="false"
				   width="600px">
			<el-form :model="${className}Model" ref="${className}Form" label-width="100px"
					 :rules="${className}FormRules" size="small">
				<#list formList as field>
				<el-form-item label="${field.fieldComment!}" prop="${field.attrName}">
					<#if field.formType == 'text'>
					<el-input v-model="${className}Model.${field.attrName}" placeholder="${field.fieldComment!}"/>
					<#elseif field.formType == 'textarea' || field.formType == 'editor'>
						<el-input type="textarea" v-model="${className}Model.${field.attrName}" placeholder="${field.fieldComment!}"/>
					<#elseif field.formType == 'select'>
						<el-select v-model="${className}Model.${field.attrName}" placeholder="请选择${field.fieldComment!}">
							<el-option
									v-for="option in optionData"
									:key="option.value"
									:label="option.label"
									:value="option.value"
							/>
						</el-select>
					<#elseif field.formType == 'radio'>
						<el-radio-group v-model="${className}Model.${field.attrName}">
							<el-radio
									v-for="option in optionData"
									:key="option.value"
									:label="option.value">
								{{option.label}}
							</el-radio>
						</el-radio-group>
					<#elseif field.formType == 'checkbox'>
						<el-checkbox-group v-model="${className}Model.${field.attrName}">
							<el-checkbox
									v-for="option in optionData"
									:key="option.value"
									:label="option.value">
								{{option.label}}
							</el-checkbox>
						</el-checkbox-group>
					<#elseif field.formType == 'date'>
						<el-date-picker
							clearable size="small"
							v-model="${className}Model.${field.attrName}"
							type="daterange"
							value-format="YYYY-MM-DD"
							placeholder="选择${field.fieldComment!}">
						</el-date-picker>
					<#elseif field.formType == 'datetime'>
						<el-date-picker
							clearable size="small"
								v-model="${className}Model.${field.attrName}"
								type="datetimerange"
								value-format="YYYY-MM-DD HH:mm:ss"
								placeholder="选择${field.fieldComment!}">
						</el-date-picker>
					</#if>
				</el-form-item>
			</#list>
			</el-form>
			<div slot="footer" class="dialog-footer">
				<el-button @click="dialog.dialogFormVisible=false" size="medium">取 消</el-button>
				<el-button type="primary" @click="submitForm" size="medium">确 定</el-button>
			</div>
		</el-dialog>
	</div>
</template>
<script lang="ts">
import {Component, Ref, Vue} from "vue-property-decorator";
import DataTable from "@/components/DataTable/DataTable.vue";

import {
	add${ClassName},
	get${ClassName}List,
	remove${ClassName},
	update${ClassName}
} from '@/api/${functionName}';

@Component({
	components: {
		DataTable
	}
})
export default class ${ClassName} extends Vue {
	private get${ClassName}List: any = get${ClassName}List;
	@Ref("dataTable") private dataTable: any;
	@Ref("selectForm") private selectForm: any;
	@Ref("${className}Form") private ${className}Form: any;
		/*搜索框*/
	selectFormModel: any = {
		<#list queryList as field>
		${field.attrName}: <#if field.attrType =='Long' || field.attrType='Integer'>null<#else>""</#if><#if field_has_next>,</#if>
		</#list>
	};
	/*列信息*/
	columns: any = [
		<#list gridList as field>
		{label: "${field.fieldComment!}", prop: "${field.attrName}"<#if field.gridSort>, sortable: "custom"</#if>}<#if field_has_next>,</#if>
		</#list>
	];
	optionData=[{value: 1,label:"测试数据"}];
	// table 的参数
	options: any = {
		stripe: true, // 是否为斑马纹 table
		loading: false, // 是否添加表格loading加载动画
		highlightCurrentRow: true, // 是否支持当前行高亮显示
		multipleSelect: true, // 是否支持列表项选中功能
		defaultSort:{
			prop: '${primaryList[0].attrName}',
			order: 'descending'
		}
	};
	/*操作栏*/
	operates: any = {
		title: "操作",
		width: "auto",
		fixed: "right",
		list: [
			{
				label: "编辑",
				type: "warning",
				icon: "el-icon-edit",
				plain: true,
				disabled: (index, row) => {
					return false;
				},
				permission: "${moduleName}:${functionName}:edit",
				method: (index, row) => {
					this.handleEdit(index, row);
				}
			},
			{
				label: "删除",
				type: "danger",
				icon: "el-icon-delete",
				plain: true,
				disabled: (index, row) => {
					return false;
				},
				permission: "${moduleName}:${functionName}:remove",
				method: (index, row) => {
					this.handleDelete(index, row);
				}
			}
		]
	};
	dialog: any = {
		dialogFormVisible: false,
		title: "对话框",
		isEdit: false,
		isBatchEditDisabled: true,
		isBatchRemoveDisabled: true,
		formData: []
	};

	${className}Model: any = {
	<#list formList as field>
		${field.attrName}: <#if field.attrType =='Long' || field.attrType='Integer'>null<#else>""</#if><#if field_has_next>,</#if>
	</#list>
	};

	${className}FormRules: any = {
		<#list formList as field>
		<#if field.formRequired??>
		${field.attrName}: [
			{required: true, message: "${field.fieldComment}不能为空", trigger: <#if field.formType == 'select'>"change"<#else>"blur"</#if>}
		]<#if field_has_next>,</#if>
		</#if>
		</#list>
	};

	/*新增*/
	handleAdd() {
		this.dialog.title = "新增${tableComment}";
		this.dialog.isEdit = false;
		this.dialog.dialogFormVisible = true;
	}

	/*编辑*/
	handleEdit(index: number, row: any) {
		this.dialog.title = "编辑${tableComment}";
		this.dialog.isEdit = true;

		this.${className}Model = Object.assign({}, row || this.dialog.formData[0]);
		<#list formList as field>
		<#if field.formType == "checkbox">
		this.${className}Model.${field.attrName} = this.${className}Model.${field.attrName}.split(",");
		</#if>
		</#list>
		this.dialog.dialogFormVisible = true;
	}

	/*删除*/
	handleDelete(index: number, row: any) {
		this.$confirm("确定要删除该${tableComment}？", "警告", {
			confirmButtonText: "确定",
			cancelButtonText: "取消",
			type: "warning"
		}).then(() => {
			return remove${ClassName}({"${primaryList[0].attrName}s": [row.${primaryList[0].attrName}]});
		}).then((response: any) => {
			this.dataTable.refresh();
			this.$message.success(response.msg);
		}).catch((e) => {
			console.error(e);
		});
	}

	/*批量删除*/
	handleBatchDelete() {
		//删除
		this.$confirm("确定要删除选定${tableComment}？", "警告", {
			confirmButtonText: "确定",
			cancelButtonText: "取消",
			type: "warning"
		}).then(() => {
			const ${primaryList[0].attrName}s: Array<any> = [];
			this.dialog.formData.forEach((item, index) => {
				${primaryList[0].attrName}s[index] = item.${primaryList[0].attrName};
			});
			return remove${ClassName}({"${primaryList[0].attrName}s": ${primaryList[0].attrName}s});
		}).then((response: any) => {
			this.dataTable.refresh();
			this.$message.success(response.msg);
		}).catch((e) => {
			console.error(e);
		});
	}

	submitForm(): void {
		this.${className}Form.validate((valid: boolean) => {
			if (valid) {
				<#list formList as field>
				<#if field.formType == "checkbox">
				this.${className}Model.${field.attrName} = this.${className}Model.${field.attrName}.split(",");
				</#if>
				</#list>
				if (this.dialog.isEdit) {
					//编辑
					update${ClassName}(this.${className}Model).then((response: any) => {
						this.dataTable.refresh();
						this.reset();
						this.dialog.dialogFormVisible = false;
						this.$message.success(response.msg);
					}).catch((e) => {
						console.error(e);
					});
				} else {
					//新增
					add${ClassName}(this.${className}Model).then((response: any) => {
						this.dataTable.refresh();
						this.reset();
						this.dialog.dialogFormVisible = false;
						this.$message.success(response.msg);
					}).catch((e) => {
						console.error(e);
					});
				}
			}
		});
	}


	/*选中事件*/
	handleSelectionChange(val): void {
		if (val) {
			this.dialog.isBatchRemoveDisabled = val.length <= 0;
			this.dialog.isBatchEditDisabled = val.length !== 1;
		}
		this.dialog.formData = val;
	}

	/*搜索*/
	onSearch(): void {
		this.dataTable.refresh();
	}

	/*重置*/
	onRefreshSelectForm(): void {
		//恢复搜索默认信息
		this.selectForm.resetFields();
		this.onSearch();
	}

	/*重置表单*/
	reset(): void {
		this.${className}Model = {
			<#list formList as field>
			${field.attrName}: <#if field.formType == "checkbox">[]<#else>""</#if><#if field_has_next>,</#if>
			</#list>
		};
		(this as any).resetForm("${className}Form");
	}

	/*关闭对话框*/
	closeDialog() {
		this.$nextTick(()=>{
			this.reset();
		});
	}
}
</script>

<style scoped lang="scss">

</style>

