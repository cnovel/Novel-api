package com.novel.system.domain.vo;

import lombok.Data;
import lombok.ToString;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * 路由菜单
 *
 * @author novel
 * @since 2019/4/11
 */
@Data
@ToString
public class MenuVo implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    /**
     * 请求url
     */
    private String path;
    /**
     * 对应组件
     */
    private String component;
    /**
     * 父级菜单重定向地址
     */
    private String redirect;
    /**
     * 路由名称
     */
    private String name;
    /**
     * 路由附带内容
     */
    private MetaVo meta;
    /**
     * 显示隐藏
     */
    private Boolean visible = true;
    /**
     * 子路由
     */
    private List<MenuVo> children;
}
