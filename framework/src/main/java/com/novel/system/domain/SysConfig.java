package com.novel.system.domain;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.novel.framework.base.BaseModel;
import com.novel.framework.validate.groups.AddGroup;
import com.novel.framework.validate.groups.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serial;
import java.util.Date;

/**
 * 参数配置表 sys_config
 *
 * @author novel
 * @since 2020/07/08
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SysConfig extends BaseModel {
    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 参数名称
     */
    @NotBlank(message = "参数名称不能为空", groups = {AddGroup.class, EditGroup.class})
    @Size(max = 100, message = "参数名称长度不能超过100个字符", groups = {AddGroup.class, EditGroup.class})
    @Excel(name = "参数名称", width = 20)
    private String configName;
    /**
     * 参数键名
     */
    @NotBlank(message = "参数键名不能为空", groups = {AddGroup.class, EditGroup.class})
    @Size(max = 100, message = "参数键名长度不能超过100个字符", groups = {AddGroup.class, EditGroup.class})
    @Excel(name = "参数键名", width = 20)
    private String configKey;
    /**
     * 参数键值
     */
    @NotBlank(message = "参数键值不能为空", groups = {AddGroup.class, EditGroup.class})
    @Size(max = 500, message = "参数键值长度不能超过500个字符", groups = {AddGroup.class, EditGroup.class})
    @Excel(name = "参数键值", width = 30)
    private String configValue;
    /**
     * 系统内置（1是 0否）
     */

    @NotNull(message = "是否系统内置不能为空", groups = {AddGroup.class, EditGroup.class})
    @Excel(name = "系统内置", replace = {"否_false", "是_true"})
    private Boolean configType;
}
