package com.novel.system.controller;


import com.novel.framework.base.BaseController;
import com.novel.framework.result.Result;
import com.novel.framework.satoken.service.SysLoginService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotBlank;

/**
 * 用户登录控制器
 *
 * @author novel
 * @since 2019/3/18
 */
@RestController
public class LoginController extends BaseController {

    private final SysLoginService sysLoginService;


    public LoginController(SysLoginService sysLoginService) {
        this.sysLoginService = sysLoginService;
    }

    /**
     * 用户登录
     *
     * @param userName            用户名
     * @param password            密码
     * @param key                 验证码key
     * @param code                验证码
     * @param rememberMe          是否记住登录
     * @return 登录结果（响应头中含authorization认证token）
     */
    @PostMapping("/login")
    public Result login(@NotBlank(message = "用户名不能为空") String userName, @NotBlank(message = "密码不能为空") String password, @NotBlank(message = "验证码key不能为空") String key, @NotBlank(message = "验证码不能为空") String code, @RequestParam(name = "rememberMe", required = false, defaultValue = "false") boolean rememberMe) {
        sysLoginService.login(userName, password, code, key, rememberMe);
        return success("登录成功！");
    }

    /**
     * 注销
     *
     * @return 注销结果
     */
    @PostMapping("/logout")
    public Result logout() {
        sysLoginService.logout();
        return success("退出成功！");
    }

}
