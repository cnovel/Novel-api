package com.novel.system.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.novel.common.constants.UserConstants;
import com.novel.common.exception.business.BusinessException;
import com.novel.framework.annotation.Log;
import com.novel.framework.base.BaseController;
import com.novel.framework.enums.BusinessType;
import com.novel.framework.result.Result;
import com.novel.framework.satoken.LoginHelper;
import com.novel.framework.validate.groups.AddGroup;
import com.novel.framework.validate.groups.EditGroup;
import com.novel.system.domain.SysMenu;
import com.novel.system.service.SysMenuService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 菜单信息
 *
 * @author novel
 * @since 2019/12/20
 */
@RestController
@RequestMapping("/system/menu")
public class SysMenuController extends BaseController {
    private final SysMenuService sysMenuService;

    public SysMenuController(SysMenuService sysMenuService) {
        this.sysMenuService = sysMenuService;
    }

    /**
     * 加载所有菜单列表树
     *
     * @return 菜单树形列表
     */
    @SaCheckPermission("system:menu:list")
    @GetMapping("/menuTreeTableData")
    public Result menuTreeTableData() {
        return toAjax(sysMenuService.menuTreeTableData());
    }

    /**
     * 加载所有菜单树
     *
     * @return 菜单树
     */
    @SaCheckPermission("system:menu:list")
    @GetMapping("/menuTreeData")
    public Result menuTreeData() {
        return toAjax(sysMenuService.menuTreeData());
    }

    /**
     * 加载所有菜单树，不含按钮
     *
     * @return 所有菜单树，不含按钮
     */
    @SaCheckPermission("system:menu:list")
    @GetMapping("/menuTreeSelectData")
    public Result menuTreeSelectData() {
        return toAjax(sysMenuService.menuTreeSelectData());
    }

    /**
     * 添加菜单信息
     *
     * @param menu 菜单信息
     * @return 操作结果
     */
    @Log(title = "菜单管理", businessType = BusinessType.INSERT)
    @SaCheckPermission("system:menu:add")
    @PostMapping("/add")
    public Result addSave(@Validated(AddGroup.class) SysMenu menu) {
        if (UserConstants.MENU_NAME_NOT_UNIQUE.equals(sysMenuService.checkMenuNameUnique(menu))) {
            throw new BusinessException("新增菜单'" + menu.getMenuName() + "'失败，菜单名称已存在");
        }

        menu.setCreateBy(LoginHelper.getUserName());
        menu.setUpdateBy(LoginHelper.getUserName());
        boolean result = sysMenuService.insertMenu(menu);
        if (result) {
//            ShiroUtils.clearCachedAuthorizationInfo();
        }
        return toAjax(result, "菜单新增成功", "菜单新增失败");
    }

    /**
     * 修改保存菜单
     *
     * @param menu 编辑的菜单信息
     * @return 操作结果
     */
    @Log(title = "菜单管理", businessType = BusinessType.UPDATE)
    @SaCheckPermission("system:menu:edit")
    @PutMapping("/edit")
    public Result editSave(@Validated(EditGroup.class) SysMenu menu) {
        if (UserConstants.MENU_NAME_NOT_UNIQUE.equals(sysMenuService.checkMenuNameUnique(menu))) {
            throw new BusinessException("新增菜单'" + menu.getMenuName() + "'失败，菜单名称已存在");
        }
        menu.setUpdateBy(LoginHelper.getUserName());
        boolean result = sysMenuService.updateMenu(menu);
        if (result) {
//            ShiroUtils.clearCachedAuthorizationInfo();
        }
        return toAjax(result, "菜单修改成功", "菜单修改失败");
    }


    /**
     * 删除菜单
     *
     * @param ids 菜单id
     * @return 操作结果
     */
    @Log(title = "菜单管理", businessType = BusinessType.DELETE)
    @SaCheckPermission("system:menu:remove")
    @DeleteMapping("/remove")
    public Result remove(Long[] ids) {
        boolean result = sysMenuService.deleteMenuByIds(ids);
        if (result) {
//            ShiroUtils.clearCachedAuthorizationInfo();
        }
        return toAjax(result, "菜单删除成功", "菜单删除失败");
    }


    /**
     * 校验菜单名称
     *
     * @param menu 包含菜单名称的菜单对象
     * @return 结果
     */
    @PostMapping("/checkMenuNameUnique")
    public Result checkMenuNameUnique(SysMenu menu) {
        return toAjax(sysMenuService.checkMenuNameUnique(menu));
    }

}