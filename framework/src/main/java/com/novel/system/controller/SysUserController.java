package com.novel.system.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.annotation.SaMode;
import com.novel.common.constants.Constants;
import com.novel.common.constants.UserConstants;
import com.novel.common.exception.business.BusinessException;
import com.novel.common.utils.StringUtils;
import com.novel.framework.annotation.Log;
import com.novel.framework.base.BaseController;
import com.novel.framework.enums.BusinessType;
import com.novel.framework.result.Result;
import com.novel.framework.satoken.LoginHelper;
import com.novel.framework.utils.excel.ExcelUtils;
import com.novel.framework.utils.spring.BeanUtils;
import com.novel.framework.validate.groups.AddGroup;
import com.novel.framework.validate.groups.EditGroup;
import com.novel.framework.web.page.TableDataInfo;
import com.novel.system.domain.SysUser;
import com.novel.system.domain.vo.SysUserVo;
import com.novel.system.service.SysConfigService;
import com.novel.system.service.SysPostService;
import com.novel.system.service.SysRoleService;
import com.novel.system.service.SysUserService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户操作控制器
 *
 * @author novel
 * @since 2020/3/2
 */
@RestController
@RequestMapping("/system/user")
public class SysUserController extends BaseController {
    private final SysUserService sysUserService;
    private final SysRoleService roleService;
    private final SysPostService postService;
    private final SysConfigService configService;

    public SysUserController(SysUserService sysUserService, SysRoleService roleService, SysPostService postService, SysConfigService configService) {
        this.sysUserService = sysUserService;
        this.roleService = roleService;
        this.postService = postService;
        this.configService = configService;
    }

    /**
     * 添加用户信息
     *
     * @param user 用户信息
     * @return 操作结果
     */
    @Log(title = "用户管理", businessType = BusinessType.INSERT)
    @SaCheckPermission("system:user:add")
    @PostMapping("/add")
    public Result addSave(@Validated(AddGroup.class) SysUser user) {
        if (UserConstants.USER_NAME_NOT_UNIQUE.equals(sysUserService.checkUserNameUnique(user))) {
            throw new BusinessException("新增用户'" + user.getUserName() + "'失败，登录账号已存在");
        } else if (UserConstants.USER_PHONE_NOT_UNIQUE.equals(sysUserService.checkPhoneUnique(user))) {
            throw new BusinessException("新增用户'" + user.getUserName() + "'失败，手机号码已存在");
        } else if (UserConstants.USER_EMAIL_NOT_UNIQUE.equals(sysUserService.checkEmailUnique(user))) {
            throw new BusinessException("新增用户'" + user.getUserName() + "'失败，邮箱账号已存在");
        }

        user.setCreateBy(LoginHelper.getUserName());
        String password = configService.selectConfigByKey(Constants.DEFAULT_PASSWORD_CONFIG_KEY);
        user.setPassword(StringUtils.isNotEmpty(password) ? password : Constants.DEFAULT_PASSWORD);
        return toAjax(sysUserService.insertUser(user), "用户新增成功", "用户新增失败");
    }

    /**
     * 删除用户信息
     *
     * @param ids 用户ID数组
     * @return 操作结果
     */
    @Log(title = "用户管理", businessType = BusinessType.DELETE)
    @SaCheckPermission("system:user:remove")
    @DeleteMapping("/remove")
    public Result remove(Long[] ids) {
        if (ids != null) {
            for (Long id : ids) {
                SysUser user = sysUserService.selectUserById(id);
                LoginHelper.logout(user);
            }
        }
        return toAjax(sysUserService.deleteUserByIds(ids), "用户删除成功", "用户删除失败");
    }

    /**
     * 保存编辑用户信息
     *
     * @param user 用户信息
     * @return 操作结果
     */
    @Log(title = "用户管理", businessType = BusinessType.UPDATE)
    @SaCheckPermission("system:user:edit")
    @PutMapping("/edit")
    public Result editSave(@Validated(EditGroup.class) SysUser user) {
        if (StringUtils.isNotNull(user.getId()) && SysUser.isAdmin(user.getId())) {
            throw new BusinessException("不允许修改超级管理员用户");
        }
        if (UserConstants.USER_PHONE_NOT_UNIQUE.equals(sysUserService.checkPhoneUnique(user))) {
            throw new BusinessException("新增用户'" + user.getUserName() + "'失败，手机号码已存在");
        } else if (UserConstants.USER_EMAIL_NOT_UNIQUE.equals(sysUserService.checkEmailUnique(user))) {
            throw new BusinessException("新增用户'" + user.getUserName() + "'失败，邮箱账号已存在");
        }
        user.setUpdateBy(LoginHelper.getUserName());
        return toAjax(sysUserService.updateUser(user), "用户修改成功！", "用户修改失败");
    }

    /**
     * 查询用户列表
     *
     * @param user 查询条件
     * @return 用户列表
     */
    @SaCheckPermission(value = {"system:user:list", "project:project:grant"}, mode = SaMode.OR)
    @GetMapping("/list")
    public TableDataInfo list(SysUser user) {
        startPage();
        List<SysUser> userList = sysUserService.selectUserList(user);
        return getDataTable(userList);
    }

    /**
     * 获取用户所拥有的角色
     *
     * @param id 用户id
     * @return 操作结果
     */
    @SaCheckPermission("system:user:edit")
    @GetMapping("/getUserRole")
    @Validated
    public Result getUserRole(@NotNull(message = "用户ID不能为空") @Min(value = 1, message = "用户ID必须大于0") Long id) {
        Map<String, Object> map = roleService.selectRolesByUserId(id);
        return Result.success(map);
    }

    /**
     * 获取用户所拥有的岗位
     *
     * @param id 用户id
     * @return 操作结果
     */
    @SaCheckPermission("system:user:edit")
    @GetMapping("/getUserPost")
    @Validated
    public Result getUserPost(@NotNull(message = "用户ID不能为空") @Min(value = 1, message = "用户ID必须大于0") Long id) {
        Map<String, Object> map = postService.selectPostsByUserId(id);
        return Result.success(map);
    }

    /**
     * 根据用户id查询岗位信息
     *
     * @param id 用户id
     * @return 岗位信息
     */
    @GetMapping("/getPostListByUserId/{id}")
    @Validated
    public Result getPostListByUserId(@PathVariable("id") @NotNull(message = "用户ID不能为空") @Min(value = 1, message = "用户ID必须大于0") Long id) {
        return toAjax(postService.selectPostListByUserId(id));
    }


    /**
     * 重置密码
     *
     * @param userId 用户id
     * @return 结果
     */
    @Log(title = "重置密码", businessType = BusinessType.UPDATE)
    @SaCheckPermission("system:user:edit")
    @PutMapping("/resetPwd/{id}")
    public Result resetPwd(@PathVariable("id") @NotNull(message = "用户ID不能为空") @Min(value = 1, message = "用户ID必须大于0") Long userId) {
        if (StringUtils.isNotNull(userId) && SysUser.isAdmin(userId)) {
            throw new BusinessException("不允许修改超级管理员用户");
        }
        String password = configService.selectConfigByKey(Constants.DEFAULT_PASSWORD_CONFIG_KEY);
        if (sysUserService.resetUserPwd(userId, StringUtils.isNotEmpty(password) ? password : Constants.DEFAULT_PASSWORD)) {
            SysUser user = sysUserService.selectUserById(userId);
            LoginHelper.logout(user);
            return success("密码设置成功");
        }
        return error("密码设置成功");
    }

    /**
     * 校验用户名
     *
     * @param user 用户信息（包含用户名）
     * @return 校验结果
     */
    @PostMapping("/checkUserNameUnique")
    public Result checkUserNameUnique(SysUser user) {
        return toAjax(sysUserService.checkUserNameUnique(user));
    }

    /**
     * 校验手机号码
     *
     * @param user 用户信息（包含用户手机）
     * @return 校验结果
     */
    @PostMapping("/checkPhoneUnique")
    public Result checkPhoneUnique(SysUser user) {
        return toAjax(sysUserService.checkPhoneUnique(user));
    }

    /**
     * 校验email邮箱
     *
     * @param user 用户信息（包含邮箱）
     * @return 结果
     */
    @PostMapping("/checkEmailUnique")
    public Result checkEmailUnique(SysUser user) {
        return toAjax(sysUserService.checkEmailUnique(user));
    }


    /**
     * 导出excel
     *
     * @param user 查询信息
     * @return 导出结果
     */
    @Log(title = "用户管理", businessType = BusinessType.EXPORT)
    @SaCheckPermission("system:user:export")
    @GetMapping("/export")
    public Result export(SysUser user) {
        startPage();
        List<SysUser> userList = sysUserService.selectUserList(user);
        String fileName = ExcelUtils.exportExcelToFile(userList, "用户信息", SysUser.class);
        Map<String, Object> map = new HashMap<>(1);
        map.put("fileName", fileName);
        return toAjax(map);
    }

    /**
     * 导入excel
     *
     * @param file 导入的excel
     * @return 导入结果
     * @throws IOException IO异常
     */
    @Log(title = "用户管理", businessType = BusinessType.IMPORT)
    @SaCheckPermission("system:user:import")
    @PostMapping("/import")
    public Result importUser(@RequestParam("file") MultipartFile file) throws IOException {
        List<SysUserVo> personVoList = ExcelUtils.importExcel(file, 1, 1, true, SysUserVo.class);
        List<SysUser> list = new ArrayList<>(personVoList.size());
        String password = configService.selectConfigByKey(Constants.DEFAULT_PASSWORD_CONFIG_KEY);
        password = StringUtils.isNotEmpty(password) ? password : Constants.DEFAULT_PASSWORD;
        for (SysUserVo sysUserVo : personVoList) {
            SysUser sysUser = new SysUser();
            BeanUtils.copyProperties(sysUserVo, sysUser);
            sysUser.setPassword(password);
            list.add(sysUser);
        }
        return toAjax(sysUserService.insertUser(list));
    }

    /**
     * 数据导入模板
     *
     * @return 导入模板
     */
    @SaCheckPermission("system:user:import")
    @GetMapping("/importTemplate")
    public Result importTemplate() {
        String fileName = ExcelUtils.exportExcelToFile(new ArrayList<SysUserVo>(), "用户导入模板", SysUserVo.class);
        Map<String, Object> map = new HashMap<>(1);
        map.put("fileName", fileName);
        return toAjax(map);
    }

}
