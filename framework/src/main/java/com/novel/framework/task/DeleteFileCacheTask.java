package com.novel.framework.task;

import cn.hutool.core.io.FileUtil;
import com.novel.framework.config.ProjectConfig;
import com.novel.framework.config.ResourceConfig;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 定时清理文件缓存
 *
 * @author novel
 * @since 2023/8/17 15:28
 */
@Component("deleteFileCacheTask")
@AllArgsConstructor
@Slf4j
public class DeleteFileCacheTask {
    private final ResourceConfig resourceConfig;

    public void run() {
        if (resourceConfig.isCacheEnable()) {
            //删除缓存
            FileUtil.clean(ProjectConfig.getProfile());
            log.info("清理文件缓存成功");
        }
    }
}
