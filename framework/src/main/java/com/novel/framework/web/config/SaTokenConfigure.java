package com.novel.framework.web.config;

import cn.dev33.satoken.interceptor.SaInterceptor;
import com.novel.framework.config.SaTokenProperties;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

@Configuration
@RequiredArgsConstructor
public class SaTokenConfigure implements WebMvcConfigurer {
    private final SaTokenProperties saTokenProperties;

    // 注册 Sa-Token 拦截器，打开注解式鉴权功能 
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 注册 Sa-Token 拦截器，打开注解式鉴权功能 
        InterceptorRegistration interceptor = registry.addInterceptor(new SaInterceptor());

        List<String> whiteList = saTokenProperties.getWhiteList();
        for (String item : whiteList) {
            interceptor.excludePathPatterns(item);
        }
        interceptor.addPathPatterns("/**");
    }
}