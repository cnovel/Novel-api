package com.novel.framework.web.page;

import com.novel.common.constants.Constants;
import com.novel.framework.utils.servlet.ServletUtils;

import java.io.Serial;
import java.io.Serializable;

/**
 * 表格数据处理
 *
 * @author novel
 * @since 2019/4/17
 */
public class TableSupport implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    /**
     * 封装分页对象
     */
    public static PageDomain getPageDomain() {
        PageDomain pageDomain = new PageDomain();
        pageDomain.setPageNum(ServletUtils.getParameterToInt(Constants.PAGE_NUM));
        pageDomain.setPageSize(ServletUtils.getParameterToInt(Constants.PAGE_SIZE));
        pageDomain.setOrderByColumn(ServletUtils.getParameter(Constants.ORDER_BY_COLUMN));
        pageDomain.setIsAsc(ServletUtils.getParameter(Constants.IS_ASC));
        return pageDomain;
    }

    public static PageDomain buildPageRequest() {
        return getPageDomain();
    }
}
