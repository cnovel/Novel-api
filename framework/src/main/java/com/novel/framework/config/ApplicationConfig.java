package com.novel.framework.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * 程序注解配置
 *
 * @author novel
 * @since 2019/6/5
 */
@Configuration
// 表示通过aop框架暴露该代理对象,AopContext能够访问
@EnableAspectJAutoProxy(exposeProxy = true)
// 指定要扫描的Mapper类的包的路径
@MapperScan({"com.*.*.mapper", "com.*.mapper"})
@EnableTransactionManagement
@EnableCaching
@Import(cn.hutool.extra.spring.SpringUtil.class)
public class ApplicationConfig {

}
