package com.novel.captcha.cache.manager;

import com.novel.captcha.cache.CaptchaCache;
import com.novel.captcha.cache.impl.RedisCaptchaCache;
import com.novel.captcha.exception.CaptchaCacheException;

/**
 * 默认缓存管理器
 *
 * @author novel
 * @since 2023/8/3 18:09
 */
public class RedisCaptchaCacheManagerImpl extends AbstractCaptchaCacheManager {

    private final RedisCaptchaCache redisCaptchaCache;

    public RedisCaptchaCacheManagerImpl(RedisCaptchaCache redisCaptchaCache) {
        this.redisCaptchaCache = redisCaptchaCache;
    }

    @Override
    CaptchaCache createCache(String key) throws CaptchaCacheException {
        try {
            redisCaptchaCache.setPrefix(key);
            return redisCaptchaCache;
        } catch (Exception e) {
            throw new CaptchaCacheException();
        }
    }
}
