package com.novel.captcha.cache.impl;

import com.novel.captcha.cache.AbstractCaptchaCache;
import com.novel.captcha.cache.config.CaptchaCacheProperties;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.concurrent.TimeUnit;

/**
 * redis实现captcha缓存
 *
 * @author novel
 * @since 2023/8/3 18:08
 */
public class RedisCaptchaCache extends AbstractCaptchaCache {
    private final StringRedisTemplate redisTemplate;


    public RedisCaptchaCache(StringRedisTemplate redisTemplate, CaptchaCacheProperties captchaCacheProperties) {
        super(captchaCacheProperties);
        this.redisTemplate = redisTemplate;
    }

    @Override
    public void saveCaptcha(String key, String code) {
        redisTemplate.opsForValue().set(getPrefix() + ":" + key, code, getCaptchaCacheProperties().getCacheTimeOut(), TimeUnit.MILLISECONDS);
    }

    @Override
    public String getCaptcha(String key) {
        return redisTemplate.opsForValue().get(getPrefix() + ":" + key);
    }

    @Override
    public void removeCaptcha(String key) {
        redisTemplate.delete(getPrefix() + ":" + key);
    }
}
