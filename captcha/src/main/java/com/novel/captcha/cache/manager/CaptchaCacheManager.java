package com.novel.captcha.cache.manager;

import com.novel.captcha.cache.CaptchaCache;
import com.novel.captcha.exception.CaptchaCacheException;

/**
 * 验证码缓存管理器
 *
 * @author novel
 * @since 2023/8/3 18:09
 */
public interface CaptchaCacheManager {
    /**
     * 获取一个缓存对象
     *
     * @param key 缓存对象的key
     * @return 验证码缓存对象
     * @throws CaptchaCacheException 验证码缓存异常
     */
    CaptchaCache getCache(String key) throws CaptchaCacheException;
}
