package com.novel.captcha.cache;

import com.novel.captcha.cache.config.CacheType;
import com.novel.captcha.cache.config.CaptchaCacheProperties;

/**
 * 验证码缓存抽象处理类
 *
 * @author novel
 * @since 2019/12/23
 */
public abstract class AbstractCaptchaCache implements CaptchaCache {
    private final CaptchaCacheProperties captchaCacheProperties;
    private String prefix;

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    protected String getPrefix() {
        return prefix;
    }

    protected CaptchaCacheProperties getCaptchaCacheProperties() {
        return captchaCacheProperties;
    }

    public AbstractCaptchaCache(CaptchaCacheProperties captchaCacheProperties) {
        this.captchaCacheProperties = captchaCacheProperties;
    }

    @Override
    public CacheType getCacheType() {
        return captchaCacheProperties.getType();
    }

    @Override
    public void init() {

    }

    @Override
    public void destroy() {
    }
}
