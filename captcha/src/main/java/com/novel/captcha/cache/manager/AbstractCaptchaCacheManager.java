package com.novel.captcha.cache.manager;

import com.novel.captcha.cache.CaptchaCache;
import com.novel.captcha.exception.CaptchaCacheException;
import org.springframework.util.StringUtils;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * 验证码缓存管理器实现类
 *
 * @author novel
 * @since 2023/8/3 18:09
 */
public abstract class AbstractCaptchaCacheManager implements CaptchaCacheManager {
    private final ConcurrentMap<String, CaptchaCache> caches = new ConcurrentHashMap<>();

    @Override
    public CaptchaCache getCache(String key) throws CaptchaCacheException {
        try {
            if (StringUtils.hasText(key)) {
                CaptchaCache captchaCache = this.caches.get(key);
                if (captchaCache == null) {
                    captchaCache = this.createCache(key);
                    CaptchaCache existing = this.caches.putIfAbsent(key, captchaCache);
                    if (existing != null) {
                        captchaCache = existing;
                    }
                }
                return captchaCache;
            } else {
                throw new CaptchaCacheException();
            }
        } catch (Exception e) {
            throw new CaptchaCacheException("Cache key cannot be null or empty.");
        }
    }

    /**
     * 创建一个验证码缓存对象
     *
     * @param key 缓存对象的key
     * @return 验证码缓存对象
     * @throws CaptchaCacheException 验证码缓存异常
     */
    abstract CaptchaCache createCache(String key) throws CaptchaCacheException;

    /**
     * 销毁一个验证码缓存对象
     *
     * @throws CaptchaCacheException 验证码缓存异常
     */
    public void destroy() throws CaptchaCacheException {
        try {
            if (!this.caches.isEmpty()) {
                for (CaptchaCache captchaCache : this.caches.values()) {
                    if (captchaCache != null) {
                        captchaCache.destroy();
                    }
                }
                this.caches.clear();
            }
        } catch (Exception e) {
            throw new CaptchaCacheException();
        }
    }
}
