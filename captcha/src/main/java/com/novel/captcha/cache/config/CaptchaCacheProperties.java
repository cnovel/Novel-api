package com.novel.captcha.cache.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 验证码存储器属性配置
 *
 * @author novel
 * @since 2023/8/3 17:31
 */
@ConfigurationProperties(prefix = CaptchaCacheProperties.PREFIX)
@Data
public class CaptchaCacheProperties {
    public static final String PREFIX = "captcha.cache";
    /**
     * 验证码默认超时时间为10分钟
     */
    private long cacheTimeOut = 10 * 60 * 1000L;
    /**
     * 缓存存储类型
     */
    private CacheType type = CacheType.SESSION;
}
