package com.novel.captcha.cache.config;

/**
 * 缓存存储类型
 *
 * @author novel
 * @since 2023/8/3 18:06
 */
public enum CacheType {
    /**
     * session存储
     */
    SESSION,
    /**
     * redis 缓存存储
     */
    REDIS
}