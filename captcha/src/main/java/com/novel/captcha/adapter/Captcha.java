package com.novel.captcha.adapter;

import com.novel.captcha.CaptchaCode;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;

/**
 * 验证码产生适配器
 *
 * @author novel
 * @since 2023/8/3 18:13
 */
public interface Captcha {

    com.wf.captcha.base.Captcha getCaptcha();

    /**
     * 渲染验证码
     *
     * @param key 验证码获取键
     * @return 验证码内容图片
     */
    String render(@NotNull String key);

    /**
     * 自动生成验证码
     *
     * @return 验证码对象
     */
    CaptchaCode render();

    /**
     * 自动写出一个图片流验证码
     *
     * @param response 请求响应
     * @return 验证码key
     */
    String writeImageCode(HttpServletResponse response);

    /**
     * 自动产生一个base64形式验证码
     *
     * @return 验证码对象
     */
    CaptchaCode writeBase64Code();

    /**
     * 校对验证码
     *
     * @param key  验证码key
     * @param code 需要验证的字符串
     * @return 是否验证成功
     */
    boolean validate(@NotNull String key, String code);

    /**
     * 根据缓存的验证码key获取对应验证码
     *
     * @param key 验证码key
     * @return 验证码
     */
    String getCaptchaCode(@NotNull String key);
}
