package com.novel.captcha.exception;

/**
 * CaptchaException
 *
 * @author novel
 * @since 2023/8/3 18:11
 */
public class CaptchaException extends RuntimeException {

    public CaptchaException() {
        super();
    }

    public CaptchaException(Throwable e) {
        super(e);
    }

    public CaptchaException(String message) {
        super(message);
    }
}
