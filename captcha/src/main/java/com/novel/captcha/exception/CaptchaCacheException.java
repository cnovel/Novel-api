package com.novel.captcha.exception;

/**
 * 验证码缓存相关异常
 *
 * @author novel
 * @since 2023/8/3 18:11
 */
public class CaptchaCacheException extends CaptchaException {
    public CaptchaCacheException() {
    }

    public CaptchaCacheException(Throwable e) {
        super(e);
    }

    public CaptchaCacheException(String message) {
        super(message);
    }
}
