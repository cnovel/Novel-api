package com.novel.captcha.exception;

/**
 * 验证码超时异常
 *
 * @author novel
 * @since 2023/8/3 18:12
 */
public class CaptchaTimeoutException extends CaptchaException {
    public CaptchaTimeoutException() {
        super("Captcha is timeout");
    }

    public CaptchaTimeoutException(String message) {
        super(message);
    }
}
