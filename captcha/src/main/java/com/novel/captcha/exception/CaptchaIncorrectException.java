package com.novel.captcha.exception;

/**
 * 验证码不正确异常
 *
 * @author novel
 * @since 2023/8/3 18:11
 */
public class CaptchaIncorrectException extends CaptchaException {
    public CaptchaIncorrectException(String message) {
        super(message);
    }

    public CaptchaIncorrectException() {
    }
}
