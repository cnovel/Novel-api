package com.novel.captcha.exception;

/**
 * 生成验证码异常
 *
 * @author novel
 * @since 2023/8/3 18:12
 */
public class CaptchaRenderException extends CaptchaException {

    public CaptchaRenderException(Exception e) {
        super(e);
    }

    public CaptchaRenderException(String message) {
        super(message);
    }

    public CaptchaRenderException() {
    }
}
