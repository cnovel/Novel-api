package com.novel.captcha.config;

import lombok.Getter;

/**
 * 验证码类型
 *
 * @author novel
 * @since 2023/8/3 17:20
 */
@Getter
public enum CaptchaType {
    /**
     * 字母PNG类型
     */
    PNG,
    /**
     * 字母GIF类型
     */
    GIF,
    /**
     * 中文类型
     */
    CHINESE_PNG,
    /**
     * 中文GIF类型
     */
    CHINESE_GIF,
    /**
     * 算术类型
     */
    ARITHMETIC_PNG
}