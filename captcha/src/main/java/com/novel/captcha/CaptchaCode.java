package com.novel.captcha;

import cn.hutool.core.codec.Base64;
import lombok.Data;
import lombok.SneakyThrows;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

/**
 * 验证码对象实体
 *
 * @author novel
 * @since 2023/8/3 17:47
 */
@Data
public class CaptchaCode {
    /**
     * 验证码图片 base64
     */
    private String base64Image;
    /**
     * 验证码
     */
    private String key;

    @SneakyThrows
    public BufferedImage createImage() {
        byte[] image = Base64.decode(base64Image.replace("data:image/png;base64,", ""));
        InputStream stream = new ByteArrayInputStream(image);
        return ImageIO.read(stream);
    }
}
