package com.novel.resources;

import cn.xuyanwu.spring.file.storage.FileStorageService;
import cn.xuyanwu.spring.file.storage.spring.EnableFileStorage;
import com.novel.common.resource.IResourceService;
import com.novel.framework.config.ResourceConfig;
import com.novel.resources.impl.ResourcesImpl;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

/**
 * 自动注入配置类
 *
 * @author novel
 * @since 2023/8/21 19:54
 */
@Configuration
@EnableFileStorage
public class ResourcesAutoConfiguration {


    @Bean
    @Order(0)
    @ConditionalOnProperty(prefix = ResourceConfig.RESOURCE_PREFIX, name = "enable", havingValue = "true")
    public IResourceService resourcesImpl(FileStorageService fileStorageService) {
        return new ResourcesImpl(fileStorageService);
    }
}
