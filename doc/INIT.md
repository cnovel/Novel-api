### [介绍文档](../README.md) | [部署文档](./BUILD.md) | 初始化文档

> 这里以本人的idea  2023.2 Ultimate 版为例，如果不是这个版本的，可以自行就行度娘，操作都大同小异，应该可以自主完成。
> 使用其他工具的，请自行百度一下吧~~~


### 下载项目 
打开项目`git`地址[Novel-api](https://gitee.com/cnovel/Novel-api)

找到`克隆/下载`，根据自己的方式，选择下载方式，本人这里通过`https`方式进行。
![下载](https://raw.gitmirror.com/lizhen789/pic/main/test/202308241108818.png "下载")
点击复制，复制项目的`git`地址。


###  导入项目
打开`idea`，如果你没有打开项目，项目下图一样，则根据图示，选择即可：
![导入项目](https://raw.gitmirror.com/lizhen789/pic/main/test/202308241109504.png "导入项目")

如果你已经打开项目，则根据以下图进行选择：
![导入](https://raw.gitmirror.com/lizhen789/pic/main/test/202308241108356.png "导入")
![导入](https://raw.gitmirror.com/lizhen789/pic/main/test/202308241110345.png "导入")

然后在`url`地址栏中填写刚刚复制的项目地址即可。

###  配置项目

如果刚导入的项目已经如下图自动配置完成了，则无需再去手动配置，如果没有，则跟着作者进行检查哪些没有配置好，需要手动配置的。
![模块配置正确图](https://raw.gitmirror.com/lizhen789/pic/main/test/202308241110780.png "模块配置正确图")

* 如果`java`模块没有正确配置，则根据下图进行手动配置：

打开设置
![打开设置](https://raw.gitmirror.com/lizhen789/pic/main/test/202308241110970.png "打开设置")

选择模块信息
![选择模块信息](https://raw.gitmirror.com/lizhen789/pic/main/test/202308241110164.png "选择模块信息")

手动选择添加想要的模块
![添加模块](https://raw.gitmirror.com/lizhen789/pic/main/test/202308241110919.png "添加模块")

到此，`java`模块配置完成。

* 如果`maven`模块没有正确配置，则根据下图进行手动添加配置：

找到没有添加的`maven`模块，找到`pom.xml`文件，此时的`pom.xml`应该是橘黄色的，右击`pom.xmnl`文件，点击`add maven project`，配置完成。

![添加maven模块](https://raw.gitmirror.com/lizhen789/pic/main/test/202308241111916.png "添加maven模块")

* 如果系统安装了多个`java`版本，切换`java`版本，如下图（`项目默认编译环境为jdk17,如果没有jdk17，请自行百度安装`）：

打开项目的设置：
![打开设置](https://raw.gitmirror.com/lizhen789/pic/main/test/202308241111452.png "打开设置")

找到project
![java设置](https://raw.gitmirror.com/lizhen789/pic/main/test/202308241111088.png "java设置")

![java设置](https://raw.gitmirror.com/lizhen789/pic/main/test/202308241111602.png "java设置")

此时应该项目模块信息正常了，如果还有其他问题，请自行度娘。。。

### 配置项目属性文件
如果首次下载项目，那么项目的配置文件会是黄色的警告色，则需要编译项目。

通过maven对项目进行编译：

![编译项目](https://raw.gitmirror.com/lizhen789/pic/main/test/202308241111274.png "编译项目")

![编译信息](https://raw.gitmirror.com/lizhen789/pic/main/test/202308241111952.png "编译信息")

编译完成后，属性文件的配置信息变成正常颜色，并且配置时会有相应的配置项提示。

具体配置信息请参考[部署文档](./BUILD.md)

### 数据库脚本

脚本介绍：

- `novel.sql`是项目核心且必须的数据库文件。
- `gen.sql`是开发时代码生成功能相关的`sql`，如果不需要这个功能，则可以不引入`generator`模块和`gen.sql`。
- `quartz.sql`是任务调度功能相关的`sql`，如果不需要这个功能，则可以不引入`quartz`模块和`quartz.sql`。
- `file.sql`是文件存储功能相关的`sql`，如果不需要这个功能，则可以不引入`resources`模块和`file.sql`。

### 运行项目

基本属性配置完成后，则就可以运行项目了，找到admin模块下的`NovelApplication.java`，右击运行。

![运行项目](https://raw.gitmirror.com/lizhen789/pic/main/test/202308241111074.png "运行项目")









