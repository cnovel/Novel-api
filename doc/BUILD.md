### [介绍文档](../README.md) | 部署文档 | [初始化文档](./INIT.md)

### 准备工作
```
JDK >= 17 (推荐17版本)
MySQL >= 8.0 (推荐8.0版本)
Maven >= 3.0
Redis >= 3.2 (推荐7.0版本)
```
其他`（非必须）`
```
阿里云对象存储OSS、腾讯云对象存储COS以及七牛云对象存储和MinIo存储
```

### 运行系统

* 将下载好的项目导入到`idea`中。
* 在数据库中创建`novel`数据库并执行`sql`文件夹下的`sql`脚本。
* 运行`admin`模块下的`NovelApplication.java`（运行前建议对整个项目进行`maven install`）。
* 直至控制台中打出启动系统，并且没有任何异常，则启动成功。

### 必要配置
1. 项目的文件存储根目录，如果是`windows`系统，请根据自己的盘符进行相应的配置。
2. 修改数据库信息。
3. 修改`redis`信息。
4. 修改资源存储配置信息`file-storage`。
5. 如果使用对象存储，则需要修改相应的配置信息。

#### mysql:

编辑`admin`模块`resources`目录下的`application-dev.yml`
```yaml
spring:
  datasource:
    druid:
      #数据库连接url
      url: 
      #数据库连接用户名
      username: 
      #数据库连接密码
      password: 
```

#### redis:
编辑`admin`模块`resources`目录下的`application-dev.yml`
```yaml
spring:
  redis:
    #redis地址
    host: 
    #redis密码
    password: 
    #redis端口
    port: 
```

#### `file-storage`文件存储配置:

编辑`admin`模块`resources`目录下的`application-dev.yml`

```yaml
spring:
#文件存储配置文档： https://spring-file-storage.xuyanwu.cn/
  file-storage: #文件存储配置
    enable: true #是否启用文件存储
    cache-enable: false #是否启用缓存
    default-platform: local-plus-1 #默认使用的存储平台
    local-plus:
      - platform: local-plus-1 # 存储平台标识
        enable-storage: true  #启用存储
        enable-access: false
        storage-path: E:\image\ # 存储路径
```


### 部署系统
1. 配置好上面的配置。
2. 在项目根目录下运行`mvn clean package -Dmaven.test.skip=true`命令，等待编译完成。
3. 把`docker`文件夹下的`jar`包放到服务器上运行即可。


### 通过docker容器部署
1. 按照上述文档将程序编译完成。
2. 将`docker`文件夹下`builddocker.sh`、`Dockerfile`和编译好的`jar`文件夹拷贝到`docker`服务器上。
3. 执行

```bash
# 添加运行权限
sudo chmod 777 builddocker.sh
# 运行脚本
./builddocker.sh
```

### 配置项加密说明
1. 在配置文件中加入`jasypt.encryptor.password`配置信息。（`这个密码是用来对配置进行加解密的秘钥，一定妥善保管`）
2. 运行`admin`模块下的`AdminApplicationTests.java`中的测试方法，把`root`字符串替换为自己想要加密的属性配置项。
3. 将得到的加密后的配置项配置到对应的配置上，且用`ENC()`包裹，例如：`ENC(XW2daxuaTftQ+F2iYPQu0g==)`，括号中间的则是配置信息。
4. 配置信息配置完后删除`jasypt.encryptor.password`配置项。
5. 在启动的环境中加入`jasypt.encryptor.password`环境信息，例如在`Dockerfile`文件中启动程序时加入了`"-Djasypt.Encryptor.Password=VLu3H58dxYAsv3TIGOueaXIXBbhbT2"`信息，这里的密码和加密时使用的一致。

### 文件服务器配置说明
如果不使用文件服务器，则把`file-storage.enable`配置为`false`或者不配置，其他项均不用配置，默认存储在程序所在的服务器上。

**注：`file-storage.enable`配置为`false`或者不配置，并不会影响对象存储的初始化，项目依然可以使用对象存储功能，所以如果你引入了对象存储，还是需要配置对象存储信息的。**

文件存储服务需要引入依赖

```xml
<!--文件模块-->
<dependency>
    <groupId>com.novel</groupId>
    <artifactId>resources</artifactId>
</dependency>
```

如果选择对象存储等，具体配置请参考 [file-storage](https://spring-file-storage.xuyanwu.cn/#/%E5%BF%AB%E9%80%9F%E5%85%A5%E9%97%A8?id=%e9%85%8d%e7%bd%ae)

### 通过jenkins部署

暂略


### 常见问题
1. 如果使用`Mac`需要修改`application-dev.yml`文件路径`profile`。
2. 如果使用`Linux`提示表不存在，设置大小写敏感配置在`/etc/my.cnf`添加`lower_case_table_names=1`，重启`MYSQL`服务。
3. 如果提示当前权限不足，无法写入文件请检查`profile`配置目录是否可读可写，或者无法访问此目录。











