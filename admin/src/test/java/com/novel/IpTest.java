package com.novel;

import com.novel.common.utils.Ip2RegionUtil;
import com.novel.framework.config.ProjectConfig;
import com.novel.framework.utils.AddressUtils;
import org.junit.jupiter.api.Test;

/**
 * @author novel
 * @since 2020/4/14
 */
public class IpTest {
    @Test
    public void test1() {
        new ProjectConfig().setAddressEnabled(true);
        System.out.println(AddressUtils.getRealAddressByLocal("101.87.85.248"));
    }

    @Test
    public void test2() {
        System.out.println(Ip2RegionUtil.find("222.190.6.235"));
    }
}
