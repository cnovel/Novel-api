package com.novel.common.utils.model;

import lombok.Data;
import org.springframework.boot.autoconfigure.cache.CacheType;

/**
 * 缓存信息
 *
 * @author novel
 * @since 2021/3/23 18:18
 */
@Data
public class Cache {
    /**
     * 内存总量
     */
    private Long total;

    /**
     * 已用内存
     */
    private Long used;

    /**
     * 模式，集群，单机
     */
    private String mode;

    /**
     * 缓存类型
     */
    private CacheType type;
    /**
     * 缓存版本
     */
    private String version;
    /**
     * os
     */
    private String os;
    /**
     * 缓存ip
     */
    private String ip;
    /**
     * 端口
     */
    private Integer port;
    /**
     * 自服务器启动以来，经过的秒数
     */
    private Long uptimeInSeconds;
    /**
     * 已连接客户端的数量
     */
    private Integer connectedClients;
    /**
     * Key数量
     */
    private Long dbSize;
    /**
     * cpu使用率
     */
    private double usedCpu;
}
